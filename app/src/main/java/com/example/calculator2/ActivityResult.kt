package com.example.calculator2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ActivityResult : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        val tv:TextView = findViewById(R.id.viewResult)
        val operasi:TextView = findViewById(R.id.operasi)
        val bundle  = intent.extras!!.getString("value")
        operasi.text = intent.extras!!.getString("operasi")
        tv.text=bundle

    }
}